<?php
include "tecnickcom/tcpdf/tcpdf.php";

$spacings = [
    '.25',
    '.5',
    '1',
    '1.5',
    '2',
    '2.5',
];

$fontSizes = [
    '12',
    '16',
];

$styles = [
    '', // regular
    'B', // bold
    'I', // italic
    'B I' // bold italic
];

$fontDir = __DIR__.'/fonts';
$fonts = [];

// Get full path of fonts
foreach(new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($fontDir)) as $file){
    if(is_file($file) && is_readable($file) && strlen($file) > 3 /* ignores the '.' and '..' entries */){
        $fonts[] = $file->getPathName();
    }
}

foreach ($fonts as $location)
{
    $pdf = new TCPDF('P', 'mm', 'a4', true, 'UTF-8', false);
    $font = TCPDF_FONTS::addTTFfont($location, 'WinAnsi', '', 96);
    $pdf->SetFont($font);
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(false);

    $pdf->StartPage();
    $pdf->SetTextColor(0,0,0);

    $x = 15;
    $y = 15;

    foreach ($fontSizes AS $size) {
        $pdf->SetFontSize((int)$size);
        for($i = 0; $i < count($spacings); $i++){
            $pdf->setFontSpacing($spacings[$i]);
            $pdf->Text($x, $y, $font . ' spacing ' . $spacings[$i]);
            $y += $size;
        }
    }

    $pdf->endPage();

    $pdfDir = __DIR__.'/pdf';
    if(!file_exists($pdfDir)){
        mkdir($pdfDir);
    }

    $filename = $pdfDir . '/font-' . $font . '.pdf';

    if(file_exists($filename)) {
        unlink($filename);
    }

    $pdf->Output( $filename , 'F');
}